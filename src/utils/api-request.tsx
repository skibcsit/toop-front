import { toopApiUrl } from '../config'

interface ErrorJson {
  error: string,
  history: Array<string>,
}

class ReductionError extends Error {
  history: Array<string>

  constructor(errorJson: ErrorJson) {
    super(errorJson.error)
    this.history = errorJson.history
  }
}

async function makeRequest(method: string, path: string, data: Object): Promise<any> {
  const url = toopApiUrl + path

  let response = null
  try {
    response = await fetch(url, {
      method: method,
      mode: 'cors',
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'},
    })
  } catch (error) { throw new Error(`Server error: ${error.message}`) }

  let json = null
  try {
    json = await response.json()
  } catch (error) { throw new Error(
    response.ok ?
      `Json parse error: ${error.message}` :
      `Response status error: ${response.status}`
  ) }

  if (!response.ok) throw new ReductionError(
    json || {error: `Response status error: ${response.status}`}
  )

  return json
}

interface CodeResponse {
  reduced: string,
  history: Array<string>,
}

async function evalCode(code: String): Promise<CodeResponse> {
  return await makeRequest('POST', 'eval', {code: code})
}

export { evalCode };
