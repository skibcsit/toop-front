import React from 'react';
import * as ApiRequest from "../utils/api-request";
import {
  Container, Row, Col, Button,
  Card, CardTitle, CardText,
  Alert, Spinner, ListGroup, ListGroupItem
} from 'reactstrap';
import AceEditor from "react-ace";


interface State {
  result: string|null,
  error: string|null,
  history: Array<string>
  code: string,
  inProgress: boolean,
}

const defaultCode = `(
  \\a => 1 + (a.i)
) [
  a = @ _ => 1,
  b = @ _ => 2,
  i = @ s => (s.a) + (s.b)
]`

const nbsp = '\u00a0'


class CodeEvaluator extends React.Component<{}, State> {
  state = {
    result: null,
    error: null,
    history: [],
    code: defaultCode,
    inProgress: false,
  }

  async evalCode() {
    try {
      const resp = await ApiRequest.evalCode(this.state.code)
      this.setState({
        error: null,
        result: resp.reduced,
        history: resp.history
      })
    } catch (error) {
      this.setState({
        error: error.message,
        history: error.history,
        result: null,
      })
    }
  }

  render() {
    const ResultTitle = (
      <CardTitle>
        Result {nbsp}
        {this.state.inProgress ?
          <Spinner type="grow" size="sm" color="primary"/> :
          (this.state.history && `in ${this.state.history.length} steps`)
        }
      </CardTitle>
    )

    const ResultMessage = (
      this.state.result && <CardText
        style={{fontSize: "16pt"}}
        className="border border-info rounded p-3"
      >
        {this.state.result}
      </CardText>
    )

    const ErrorMessage = (
      this.state.error &&
        <Alert color="danger">
          <pre>{this.state.error}</pre>
        </Alert>
    )

    const EvalButton = (
      <Button
        outline
        color="success"
        onClick={() => {
          this.setState({inProgress: true})
          this.evalCode().finally(() =>
            this.setState({inProgress: false})
          )
        }}
        disabled={this.state.inProgress}
      >Eval</Button>
    )

    const History = (
      this.state.history && <div style={{overflowY: "auto", maxHeight: "70%"}}>
        <ListGroup>
          {this.state.history.map(term => <ListGroupItem>{term}</ListGroupItem>)}
        </ListGroup>
      </div>
    )

    const EvalCanvas = (
      <div style={{height: "100%"}}>
        <Card body>
          {ResultTitle}
          {ResultMessage}
          {ErrorMessage}
          {EvalButton}
        </Card>
        {History}
      </div>
    )

    const EditorCanvas = (
      <AceEditor
        onChange={code => this.setState({code: code})}
        fontSize={16}
        height="100%"
        width="100%"
        showPrintMargin={true}
        showGutter={true}
        highlightActiveLine={true}
        value={this.state.code}
        setOptions={{
          enableBasicAutocompletion: false,
          enableLiveAutocompletion: false,
          enableSnippets: false,
          showLineNumbers: true,
          tabSize: 2,
        }}
      />
    )

    return (
      <Container style={{height: "100%"}}>
        <Row style={{height: "100%"}}>
          <Col md={6} style={{height: "100%"}}>
            {EditorCanvas}
          </Col>
          <Col md={6} style={{height: "100%"}}>
            {EvalCanvas}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default CodeEvaluator;
