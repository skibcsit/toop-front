import React from "react";

const abadiLink = "http://lucacardelli.name/Talks/1996-10%20A%20Theory%20of%20Objects%20(OOPSLA%20Tutorial).pdf"

const syntax = [
  {code: "[l = @ self => expr]", desc: "object creation ( 'σ' replaced with '@' )"},
  {code: "[l = @ self => \\ a => expr]", desc: "method with one parameter ( 'λ' replaced with '\\' )"},
  {code: "[].x <= @ self => expr", desc: "method update ( '⇐' replaced with '<=' )"},
  {code: "[].x := expr", desc: "field update ( syntax sugar for [].x <= @ self => expr )"},
  {code: "42", desc: "integer constants support"},
  {code: "1 - 2 + 3", desc: "addition and substruction support"},
  {code: "1 - (1 + 2)", desc: "brackets support"},
]

const exampleCode = `(
  [
    x = @ _ => 0,
    move = @ this => \\ dx => this.x := this.x + dx
  ].move 5
).x`

const About = () => (
  <div>
    <h3 className="text-center font-weight-bold">
      This is an implementation of <a href={abadiLink} target="blank">SIGMA calculus</a>
    </h3>
    <div>
      <h4>Syntax is</h4>
      <ul>
        {syntax.map(({code, desc}, idx) =>
          <li key={idx}>
            <p>
              <code>{code}</code> - {desc}
            </p>
          </li>
        )}
      </ul>
      <h4>Example</h4>
      <pre>{exampleCode}</pre>
    </div>
  </div>
)

export default About;
