import React, { useState } from 'react';
import CodeEvaluator from "./code-evaluator";
import About from "./about";
import {
  Container,
  Navbar, NavbarBrand,
  Nav, NavItem, NavLink,
  TabContent, TabPane,
} from 'reactstrap';

enum Tab {
  Evaluator,
  About,
}

const App = () => {

  const [tab, setTab] = useState(Tab.Evaluator)

  const toggleTab = (newTab: Tab) => {
    if (newTab !== tab) setTab(newTab)
  }

  const navbar = (
    <Navbar color="light" light>
      <NavbarBrand href="/">MEPhI TOOP Project</NavbarBrand>
      <Nav className="mr-auto" tabs>
        <NavItem>
          <NavLink
            className={tab === Tab.Evaluator ? "active" : ""}
            onClick={() => toggleTab(Tab.Evaluator)}
          >
            Evaluator
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={tab === Tab.About ? "active" : ""}
            onClick={() => toggleTab(Tab.About)}
          >
            About
          </NavLink>
        </NavItem>
      </Nav>
    </Navbar>
  )

  const tabs = (
    <TabContent activeTab={tab}  style={{height: "90%"}}>
      <TabPane tabId={Tab.Evaluator} style={{height: "100%"}}>
        <CodeEvaluator/>
      </TabPane>
      <TabPane tabId={Tab.About} style={{height: "100%"}}>
        <About/>
      </TabPane>
    </TabContent>
  )

  return (
    <>
      <header>{navbar}</header>
      <Container className="bg-white" style={{
        height: "100vh"
      }}>
        {tabs}
      </Container>
    </>
  )
}

export default App;
