FROM nginx:alpine

COPY . /usr/share/nginx/html

CMD sed -i "s/listen\s*80;/listen $PORT;/g" /etc/nginx/conf.d/default.conf && \
    exec nginx -g "daemon off;"
