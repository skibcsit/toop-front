HEROKU_APP=mephi-toop


dev_up:
	npm start


heroku_deploy: heroku_push heroku_release

heroku_push:
	npm run build
	heroku container:push --context-path build -a $(HEROKU_APP) web

heroku_release:
	heroku container:release -a $(HEROKU_APP) web

heroku_log:
	heroku logs -a $(HEROKU_APP) --tail
